import random
from generador import Verificador


mapa=[
    [0,0,0],
    [0,0,0],
    [0,0,0]
]

mina = [random.randint(4, 5) for x in mapa]
 
mina.sort()
print (mina)

miVerificador=Verificador()

#Si algun verificador es verdadero se reinicia la aplicación
if (miVerificador.verificarColumnas() or miVerificador.verificarFilas()){
    return 0
}